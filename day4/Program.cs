﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

var lines = File.ReadAllLines("input");
var drawnNumbers = lines.First().Split(',').Select(uint.Parse);
var boardNumbers = lines.Skip(2).Chunk(6).
    Select(chunk => string.Join(' ', chunk)).
    Select(s => s.Split(' ')).
    Select(sa => sa.
        Where(s => s != string.Empty).
        Select(uint.Parse)
    );

List<Dictionary<uint, bool>> BoardFromNumbers(IEnumerable<uint> numbers)
{
    var board = new List<Dictionary<uint, bool>>();

    foreach (var rowNumbers in numbers.Chunk(5)) 
        board.Add(rowNumbers.ToDictionary(n => n, _ => false));

    for (var colNo = 0; colNo < 5; colNo++)
    {
        var colNumbers = numbers.Where((_, i) => i % 5 == colNo);
        board.Add(colNumbers.ToDictionary(n => n, _ => false));
    }

    return board;
}

var boards = boardNumbers.Select(BoardFromNumbers);

bool PlayBoard(List<Dictionary<uint, bool>> board, uint n, out Dictionary<uint, bool>? winningRowOrCol)
{
    winningRowOrCol = null;
    
    for (var rowOrColNo = 0; rowOrColNo < board.Count; rowOrColNo++)
    {
        var rowOrCol = board[rowOrColNo];
        if (rowOrCol.ContainsKey(n)) 
            board[rowOrColNo][n] = true;

        if (rowOrCol.All(pair => pair.Value))
            winningRowOrCol = rowOrCol;
    }

    return winningRowOrCol != null;
}

var playedNumbers = new List<uint>();
foreach (var n in drawnNumbers)
{
    playedNumbers.Add(n);
    var anyBoardWon = false;
    foreach (var board in boards)
    {
        var boardWon = PlayBoard(board, n, out var winningRowOrCol);
        anyBoardWon = anyBoardWon || boardWon;
        if (boardWon)
        {
            Console.WriteLine($"Board won after drawing sequence {string.Join(", ", playedNumbers)}: {string.Join(", ", winningRowOrCol.Keys)}");
        }
    }

    if (anyBoardWon)
        break;
}
Console.WriteLine("!!!!!!!!!!!!!");