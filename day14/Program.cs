﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

var lines = File.ReadAllLines("input");
Dictionary<(char left, char right), char> insertionRules = lines.Skip(2).ToDictionary(l => (l[0], l[1]), l => l[6]);
var template = lines.First();

var result = string.Copy(template);
var resultBuilder = new StringBuilder(result);
for (var i = 0; i < 10; i++)
{
    foreach (var (index, rule) in Enumerable.Range(0, result.Length).Zip(result.Zip(result.Skip(1))))
    {
        var containsRule = insertionRules.TryGetValue(rule, out var insertion);
        if (containsRule)
            resultBuilder.Insert(index, insertion);
    }
    
    result = resultBuilder.ToString();
    Console.WriteLine(result);
    resultBuilder.Clear();
}