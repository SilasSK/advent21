using System.Collections.Generic;
using System.Linq;

namespace day1
{
    public static class ExtensionMethods
    {
        public static IEnumerable<List<T>> SlidingWindow<T>(this IEnumerable<T> enumerable, int length)
        {
            using var windowEndEnumerator = enumerable.Skip(length).GetEnumerator();
            
            var items = new Queue<T>(enumerable.Take(length));

            while (windowEndEnumerator.MoveNext())
            {
                yield return items.ToList();
                
                if (items.Count == length)
                    items.Dequeue();
                items.Enqueue(windowEndEnumerator.Current);
            }
            
            yield return items.ToList();
        }
    }
}