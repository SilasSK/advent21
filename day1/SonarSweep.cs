﻿
using System;
using System.IO;
using System.Linq;
using day1;

class SonarSweep
{
    private static void Main()
    {
        var depths = File.ReadAllLines("input").Select(int.Parse).ToList();
        var twoDeltas = depths.SlidingWindow(2).Select(d => d[1] - d[0]);

        Console.WriteLine($"Part 1 answer: {twoDeltas.Count(d => d > 0)}");

        var threeSums = depths.SlidingWindow(3).Select(d => d.Sum());
        var threeDeltas = threeSums.SlidingWindow(2).Select(d => d[1] - d[0]);
        
        Console.WriteLine($"Part 2 answer: {threeDeltas.Count(d => d > 0)}");
    }
}