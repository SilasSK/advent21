using System.Collections.Generic;

namespace day2
{
    public abstract class Sub
    {
        protected (int X, int Y) _position;

        public (int X, int Y) Position
        {
            get => _position;
            set => _position = value;
        }

        public Sub((int X, int Y) position)
        {
            Position = position;
        }

        public Sub() : this((0, 0)) {}

        public abstract void Move((int X, int Y) movement);
        
        public abstract void FollowPath(IEnumerable<(int X, int Y)> movements);
    }
}