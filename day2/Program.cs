using System;
using System.IO;
using System.Linq;

namespace day2
{
    public class Program
    {
        private static (int X, int Y) MovementFromString(string s)
        {
            var speed = int.Parse(s[s.IndexOf(' ')..]);
            return s[0] switch
            {
                'f' => (speed, 0),
                'd' => (0, speed),
                'u' => (0, -speed),
                _ => throw new ArgumentException()
            };
        }
        
        private static void Main()
        {
            var movements = File.ReadAllLines("input").Select(MovementFromString);
            
            var part1Sub = new Part1Sub();
            part1Sub.FollowPath(movements);
            
            Console.WriteLine($"Part 1 answer: sub is at {part1Sub.Position}, product is {part1Sub.Position.X * part1Sub.Position.Y}");
            
            var part2Sub = new Part2Sub();
            part2Sub.FollowPath(movements);
            
            Console.WriteLine($"Part 2 answer: sub is at {part2Sub.Position}, product is {part2Sub.Position.X * part2Sub.Position.Y}");
        }
    }
}