using System;
using System.Collections.Generic;

namespace day2
{
    class Part2Sub : Sub
    {
        private int _aim;

        public override void Move((int X, int Y) movement)
        {
            if (movement.Y != 0 && movement.X == 0)
                _aim += movement.Y;
            else if (movement.X != 0 && movement.Y == 0)
            {
                _position.X += movement.X;
                _position.Y += _aim * movement.X;
            }
            else
                throw new ArgumentException();

        }
        
        public override void FollowPath(IEnumerable<(int X, int Y)> movements)
        {
            foreach (var m in movements)
                Move(m);
        }

        
    }
}