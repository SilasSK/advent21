﻿using System.Collections.Generic;

namespace day2
{
    class Part1Sub : Sub
    {
        public override void Move((int X, int Y) movement)
        {
            _position.X += movement.X;
            _position.Y += movement.Y;
        }
        
        public override void FollowPath(IEnumerable<(int X, int Y)> movements)
        {
            foreach (var m in movements)
                Move(m);
        }
    }
}