﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

ulong FromBits(IEnumerable<bool> bits)
{
    var bit = 0;
    return (uint)bits.Sum(b => !b ? 0 * bit++ : (uint)Math.Pow(2, bit++));
}

ulong FromBitString(string s) => FromBits(s.Reverse().Select(c => c == '1'));

bool BitSet(ulong n, int bit) => 0 != (n & (ulong)1 << bit);

var inputLines = File.ReadAllLines("input");
var measurements = inputLines.Select(FromBitString);
var numBits = inputLines.First().Length;

int[] CountSetBits(IEnumerable<ulong> numbers)
{
    var r = new int[numBits];
    foreach (var m in numbers)
        for (var bit = 0; bit < numBits; bit++)
            r[bit] += BitSet(m, bit) ? 1 : 0;
    return r;
}

List<int> Bits(ulong n)
{
    var r = new List<int>();
    for (var bit = 0; bit < numBits; bit++) 
        r.Add(BitSet(n, bit) ? 1 : 0);
    return r;
}

var setCounts = CountSetBits(measurements);
var ΓRate = FromBits( setCounts.Select(c => c > inputLines.Length / 2) );
var εRate = FromBits( setCounts.Select(c => c <= inputLines.Length / 2) );

Console.WriteLine($"Part 1 answer: {εRate * ΓRate}");

string ToBitString(ulong n)
{
    string s = "";
    for (int bit = 0; bit < numBits; bit++)
    {
        s = (BitSet(n, bit) ? '1' : '0') + s;
    }
    return s;
}

var oxygenMeasurements = measurements.ToList();

Console.WriteLine($"Oxygen: {string.Join(", ", oxygenMeasurements.Select(ToBitString))}");

for (var bit = numBits - 1; bit >= 0; bit--)
{
    var setCount= oxygenMeasurements.Count(m => BitSet(m, bit));
    var numMeasurements = oxygenMeasurements.Count;
    var target = setCount > numMeasurements / 2 || setCount == numMeasurements / 2;
    Console.WriteLine(target);
    oxygenMeasurements.RemoveAll(m =>
    {
        var bitSet = BitSet(m, bit); 
        return bitSet != target;
    });
    Console.WriteLine($"Oxygen: {string.Join(", ", oxygenMeasurements.Select(ToBitString))}");
    
    if (oxygenMeasurements.Count == 1)
        break;
}


